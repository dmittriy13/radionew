<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 15.12.17
 * Time: 2:34
 */

namespace AppBundle\Controller;


use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RadioController extends  Controller
{
    /**
     * @Route("/", name = "radio")
     *
     */

    public function showAction() {

        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('@App/Radio/show.html.twig', [
            'user' => $user,
        ]);
    }



}