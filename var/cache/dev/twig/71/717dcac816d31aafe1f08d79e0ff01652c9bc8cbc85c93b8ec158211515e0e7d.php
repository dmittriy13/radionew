<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_b5cbb50e91384bd890599fb144479580974df60c58c423de6448a48c534963fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_254ed3dd2eef194a244582d6d011c5713dbb2d1785edd3f477157d7266981e60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_254ed3dd2eef194a244582d6d011c5713dbb2d1785edd3f477157d7266981e60->enter($__internal_254ed3dd2eef194a244582d6d011c5713dbb2d1785edd3f477157d7266981e60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_2b5515c5e964895c73152947859df7d5f49bc3842da148bd082d80102f2e3553 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b5515c5e964895c73152947859df7d5f49bc3842da148bd082d80102f2e3553->enter($__internal_2b5515c5e964895c73152947859df7d5f49bc3842da148bd082d80102f2e3553_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_254ed3dd2eef194a244582d6d011c5713dbb2d1785edd3f477157d7266981e60->leave($__internal_254ed3dd2eef194a244582d6d011c5713dbb2d1785edd3f477157d7266981e60_prof);

        
        $__internal_2b5515c5e964895c73152947859df7d5f49bc3842da148bd082d80102f2e3553->leave($__internal_2b5515c5e964895c73152947859df7d5f49bc3842da148bd082d80102f2e3553_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9f802079b132768777e058301ee2dfc9bc1b20069d215114f1e5934ac83af410 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f802079b132768777e058301ee2dfc9bc1b20069d215114f1e5934ac83af410->enter($__internal_9f802079b132768777e058301ee2dfc9bc1b20069d215114f1e5934ac83af410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_90af987b311661322105de883c9a67ec0c4146034d99a7aa0de94c0e54513f0e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90af987b311661322105de883c9a67ec0c4146034d99a7aa0de94c0e54513f0e->enter($__internal_90af987b311661322105de883c9a67ec0c4146034d99a7aa0de94c0e54513f0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_90af987b311661322105de883c9a67ec0c4146034d99a7aa0de94c0e54513f0e->leave($__internal_90af987b311661322105de883c9a67ec0c4146034d99a7aa0de94c0e54513f0e_prof);

        
        $__internal_9f802079b132768777e058301ee2dfc9bc1b20069d215114f1e5934ac83af410->leave($__internal_9f802079b132768777e058301ee2dfc9bc1b20069d215114f1e5934ac83af410_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
