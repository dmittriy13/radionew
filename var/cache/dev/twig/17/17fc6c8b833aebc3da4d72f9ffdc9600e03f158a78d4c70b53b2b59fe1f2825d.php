<?php

/* @App/Radio/show_content.html.twig */
class __TwigTemplate_5a13d54f21027ea9fcfe41769ac9f34dbb0ca9563b98b0fb099c87e7e440db00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_383ce64c084d019e3f655c08a422e887374e6c164a6f97f82ec63a28fb217b98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_383ce64c084d019e3f655c08a422e887374e6c164a6f97f82ec63a28fb217b98->enter($__internal_383ce64c084d019e3f655c08a422e887374e6c164a6f97f82ec63a28fb217b98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Radio/show_content.html.twig"));

        $__internal_0b742b553c89a710882dd70d829bad1dc4125b6345cf02e0002dfa61806bd2d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b742b553c89a710882dd70d829bad1dc4125b6345cf02e0002dfa61806bd2d3->enter($__internal_0b742b553c89a710882dd70d829bad1dc4125b6345cf02e0002dfa61806bd2d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Radio/show_content.html.twig"));

        // line 1
        echo "<div id=\"stats\">
    <p>statistics</p>
    <p>username: ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>email: ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>";
        
        $__internal_383ce64c084d019e3f655c08a422e887374e6c164a6f97f82ec63a28fb217b98->leave($__internal_383ce64c084d019e3f655c08a422e887374e6c164a6f97f82ec63a28fb217b98_prof);

        
        $__internal_0b742b553c89a710882dd70d829bad1dc4125b6345cf02e0002dfa61806bd2d3->leave($__internal_0b742b553c89a710882dd70d829bad1dc4125b6345cf02e0002dfa61806bd2d3_prof);

    }

    public function getTemplateName()
    {
        return "@App/Radio/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"stats\">
    <p>statistics</p>
    <p>username: {{ user.username }}</p>
    <p>email: {{ user.email }}</p>
</div>", "@App/Radio/show_content.html.twig", "/home/dmitriy/study/radionew/src/AppBundle/Resources/views/Radio/show_content.html.twig");
    }
}
