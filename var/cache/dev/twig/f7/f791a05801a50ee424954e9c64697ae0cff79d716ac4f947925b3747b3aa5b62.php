<?php

/* @App/Radio/show.html.twig */
class __TwigTemplate_ef68746a05313a7d0d4cc4f6952ea8b9ff0dfad02f4351722f1d15b4d28a6c30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@App/layout.html.twig", "@App/Radio/show.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@App/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40996dbf82d9173d7b6276f9b06de96ad7a50df9ebe10a14edc32f697f1976cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40996dbf82d9173d7b6276f9b06de96ad7a50df9ebe10a14edc32f697f1976cc->enter($__internal_40996dbf82d9173d7b6276f9b06de96ad7a50df9ebe10a14edc32f697f1976cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Radio/show.html.twig"));

        $__internal_a44cada6b91618181d5f16585304043621dab6209d4e2940b1cf1fb605436970 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a44cada6b91618181d5f16585304043621dab6209d4e2940b1cf1fb605436970->enter($__internal_a44cada6b91618181d5f16585304043621dab6209d4e2940b1cf1fb605436970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Radio/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40996dbf82d9173d7b6276f9b06de96ad7a50df9ebe10a14edc32f697f1976cc->leave($__internal_40996dbf82d9173d7b6276f9b06de96ad7a50df9ebe10a14edc32f697f1976cc_prof);

        
        $__internal_a44cada6b91618181d5f16585304043621dab6209d4e2940b1cf1fb605436970->leave($__internal_a44cada6b91618181d5f16585304043621dab6209d4e2940b1cf1fb605436970_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_2d2a648f103f55f3c20fa58d753d21736a9d921492c9042c3e4bd94ea5c6ff64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d2a648f103f55f3c20fa58d753d21736a9d921492c9042c3e4bd94ea5c6ff64->enter($__internal_2d2a648f103f55f3c20fa58d753d21736a9d921492c9042c3e4bd94ea5c6ff64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_65d565697fdfbf1e432048dd61db1a40b91112e87f6615a9e8b819968dece592 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65d565697fdfbf1e432048dd61db1a40b91112e87f6615a9e8b819968dece592->enter($__internal_65d565697fdfbf1e432048dd61db1a40b91112e87f6615a9e8b819968dece592_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Radio online
";
        
        $__internal_65d565697fdfbf1e432048dd61db1a40b91112e87f6615a9e8b819968dece592->leave($__internal_65d565697fdfbf1e432048dd61db1a40b91112e87f6615a9e8b819968dece592_prof);

        
        $__internal_2d2a648f103f55f3c20fa58d753d21736a9d921492c9042c3e4bd94ea5c6ff64->leave($__internal_2d2a648f103f55f3c20fa58d753d21736a9d921492c9042c3e4bd94ea5c6ff64_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9fbd8b4bca62d806bdbc472904b4ac929a18f1b8d8a490a2ffed063782f9f05e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fbd8b4bca62d806bdbc472904b4ac929a18f1b8d8a490a2ffed063782f9f05e->enter($__internal_9fbd8b4bca62d806bdbc472904b4ac929a18f1b8d8a490a2ffed063782f9f05e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1e43fa1e9e0b518348fac1e17c5869ac83386201439d186695ffe5c5574e4a0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e43fa1e9e0b518348fac1e17c5869ac83386201439d186695ffe5c5574e4a0b->enter($__internal_1e43fa1e9e0b518348fac1e17c5869ac83386201439d186695ffe5c5574e4a0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1e43fa1e9e0b518348fac1e17c5869ac83386201439d186695ffe5c5574e4a0b->leave($__internal_1e43fa1e9e0b518348fac1e17c5869ac83386201439d186695ffe5c5574e4a0b_prof);

        
        $__internal_9fbd8b4bca62d806bdbc472904b4ac929a18f1b8d8a490a2ffed063782f9f05e->leave($__internal_9fbd8b4bca62d806bdbc472904b4ac929a18f1b8d8a490a2ffed063782f9f05e_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_2ac854d5b36a620b66e61ff0865ba2fc2dcb378ffb6eb1a4b44e1bb638c143e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ac854d5b36a620b66e61ff0865ba2fc2dcb378ffb6eb1a4b44e1bb638c143e7->enter($__internal_2ac854d5b36a620b66e61ff0865ba2fc2dcb378ffb6eb1a4b44e1bb638c143e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_ae985615cc9848f989188496b999690456bc3018ff60be534ce5e18313a9fd0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae985615cc9848f989188496b999690456bc3018ff60be534ce5e18313a9fd0b->enter($__internal_ae985615cc9848f989188496b999690456bc3018ff60be534ce5e18313a9fd0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "    ";
        $this->loadTemplate("@App/Radio/show_content.html.twig", "@App/Radio/show.html.twig", 11)->display($context);
        
        $__internal_ae985615cc9848f989188496b999690456bc3018ff60be534ce5e18313a9fd0b->leave($__internal_ae985615cc9848f989188496b999690456bc3018ff60be534ce5e18313a9fd0b_prof);

        
        $__internal_2ac854d5b36a620b66e61ff0865ba2fc2dcb378ffb6eb1a4b44e1bb638c143e7->leave($__internal_2ac854d5b36a620b66e61ff0865ba2fc2dcb378ffb6eb1a4b44e1bb638c143e7_prof);

    }

    // line 14
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_43f83d091dfa93a1d5f795ff40a11f5a9a9d3fa92080450f73ddc4b1ebb7543b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43f83d091dfa93a1d5f795ff40a11f5a9a9d3fa92080450f73ddc4b1ebb7543b->enter($__internal_43f83d091dfa93a1d5f795ff40a11f5a9a9d3fa92080450f73ddc4b1ebb7543b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_3acaa6ab682eebf8145402c233a35e2002851abb32ae60fe61e0a78b37e2edc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3acaa6ab682eebf8145402c233a35e2002851abb32ae60fe61e0a78b37e2edc7->enter($__internal_3acaa6ab682eebf8145402c233a35e2002851abb32ae60fe61e0a78b37e2edc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 15
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
    <script>
        \$.ajax({
            url: 'http://localhost:8000/status-json.xsl',
            data: {},
            type: 'GET',
            success: function (data) {
                var stats = data;

                var sources = stats.icestats.source;

                for(var i = 0, len = sources.length; i < len; i++) {
                    \$('<audio>', {src: sources[i].listenurl, controls: true}).appendTo('#stats');
                }
            }
        });
    </script>

";
        
        $__internal_3acaa6ab682eebf8145402c233a35e2002851abb32ae60fe61e0a78b37e2edc7->leave($__internal_3acaa6ab682eebf8145402c233a35e2002851abb32ae60fe61e0a78b37e2edc7_prof);

        
        $__internal_43f83d091dfa93a1d5f795ff40a11f5a9a9d3fa92080450f73ddc4b1ebb7543b->leave($__internal_43f83d091dfa93a1d5f795ff40a11f5a9a9d3fa92080450f73ddc4b1ebb7543b_prof);

    }

    public function getTemplateName()
    {
        return "@App/Radio/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 15,  100 => 14,  89 => 11,  80 => 10,  63 => 7,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@App/layout.html.twig\" %}

{% block title %}
    Radio online
{% endblock title %}

{% block stylesheets %}
{% endblock stylesheets %}

{% block content %}
    {% include \"@App/Radio/show_content.html.twig\" %}
{% endblock content %}

{% block javascripts %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
    <script>
        \$.ajax({
            url: 'http://localhost:8000/status-json.xsl',
            data: {},
            type: 'GET',
            success: function (data) {
                var stats = data;

                var sources = stats.icestats.source;

                for(var i = 0, len = sources.length; i < len; i++) {
                    \$('<audio>', {src: sources[i].listenurl, controls: true}).appendTo('#stats');
                }
            }
        });
    </script>

{% endblock javascripts %}

", "@App/Radio/show.html.twig", "/home/dmitriy/study/radionew/src/AppBundle/Resources/views/Radio/show.html.twig");
    }
}
