<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_c9c310ed2d4989158f4c6fbe19b3b79a4b292f41f53c8f2595fb6aa0ab9782a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b4962e360e474a8673c4789cdc3f83499a99830649bbbece0c86805fc214f96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b4962e360e474a8673c4789cdc3f83499a99830649bbbece0c86805fc214f96->enter($__internal_7b4962e360e474a8673c4789cdc3f83499a99830649bbbece0c86805fc214f96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_02b134413b06898d080a33fb86e4b8e58ddb215d9c195175dd13062fdf6fca7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02b134413b06898d080a33fb86e4b8e58ddb215d9c195175dd13062fdf6fca7c->enter($__internal_02b134413b06898d080a33fb86e4b8e58ddb215d9c195175dd13062fdf6fca7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7b4962e360e474a8673c4789cdc3f83499a99830649bbbece0c86805fc214f96->leave($__internal_7b4962e360e474a8673c4789cdc3f83499a99830649bbbece0c86805fc214f96_prof);

        
        $__internal_02b134413b06898d080a33fb86e4b8e58ddb215d9c195175dd13062fdf6fca7c->leave($__internal_02b134413b06898d080a33fb86e4b8e58ddb215d9c195175dd13062fdf6fca7c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_57e6195b0b39e532e9dc6136b1877be74cad5de399421db92077bd1efcf8f302 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57e6195b0b39e532e9dc6136b1877be74cad5de399421db92077bd1efcf8f302->enter($__internal_57e6195b0b39e532e9dc6136b1877be74cad5de399421db92077bd1efcf8f302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c45d45d3fb82e2fe248b69af51ef872bb4451c157e14d025214babf375d2c637 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c45d45d3fb82e2fe248b69af51ef872bb4451c157e14d025214babf375d2c637->enter($__internal_c45d45d3fb82e2fe248b69af51ef872bb4451c157e14d025214babf375d2c637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_c45d45d3fb82e2fe248b69af51ef872bb4451c157e14d025214babf375d2c637->leave($__internal_c45d45d3fb82e2fe248b69af51ef872bb4451c157e14d025214babf375d2c637_prof);

        
        $__internal_57e6195b0b39e532e9dc6136b1877be74cad5de399421db92077bd1efcf8f302->leave($__internal_57e6195b0b39e532e9dc6136b1877be74cad5de399421db92077bd1efcf8f302_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
