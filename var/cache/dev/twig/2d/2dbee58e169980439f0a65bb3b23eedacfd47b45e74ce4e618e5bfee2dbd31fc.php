<?php

/* @FOSUser/Profile/show_content.html.twig */
class __TwigTemplate_8ea3f0e73925a85880e8ccde0ee2b78d788398f4ee13398666f8eb8c7703e36b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26b0bf77337f4b24724e2b4ef120b4d25342aa04a74fe747c00f6ef411b8f3e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26b0bf77337f4b24724e2b4ef120b4d25342aa04a74fe747c00f6ef411b8f3e9->enter($__internal_26b0bf77337f4b24724e2b4ef120b4d25342aa04a74fe747c00f6ef411b8f3e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        $__internal_04d7945463cab0bd94f65288bf6110f9c86a1b688e07a553c60bbe7069d45e5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04d7945463cab0bd94f65288bf6110f9c86a1b688e07a553c60bbe7069d45e5f->enter($__internal_04d7945463cab0bd94f65288bf6110f9c86a1b688e07a553c60bbe7069d45e5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
    <audio src=\"http://audioserver.dev:8000/ices\" autoplay controls></audio>
</div>
";
        
        $__internal_26b0bf77337f4b24724e2b4ef120b4d25342aa04a74fe747c00f6ef411b8f3e9->leave($__internal_26b0bf77337f4b24724e2b4ef120b4d25342aa04a74fe747c00f6ef411b8f3e9_prof);

        
        $__internal_04d7945463cab0bd94f65288bf6110f9c86a1b688e07a553c60bbe7069d45e5f->leave($__internal_04d7945463cab0bd94f65288bf6110f9c86a1b688e07a553c60bbe7069d45e5f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
    <audio src=\"http://audioserver.dev:8000/ices\" autoplay controls></audio>
</div>
", "@FOSUser/Profile/show_content.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}
