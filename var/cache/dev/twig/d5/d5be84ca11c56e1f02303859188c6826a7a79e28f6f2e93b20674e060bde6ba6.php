<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_4e3271903e33ece75271e854a17246dc43a169ac90a69a863843468c5ada6dd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2d9668e25bbe29261e2b30632a4309ff92d66eb4d07b9806106451edba30c21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2d9668e25bbe29261e2b30632a4309ff92d66eb4d07b9806106451edba30c21->enter($__internal_b2d9668e25bbe29261e2b30632a4309ff92d66eb4d07b9806106451edba30c21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8602462e1c5dbf9160d636f44874ec728cf55a7c7236d752bf3ef53e052e5bdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8602462e1c5dbf9160d636f44874ec728cf55a7c7236d752bf3ef53e052e5bdf->enter($__internal_8602462e1c5dbf9160d636f44874ec728cf55a7c7236d752bf3ef53e052e5bdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b2d9668e25bbe29261e2b30632a4309ff92d66eb4d07b9806106451edba30c21->leave($__internal_b2d9668e25bbe29261e2b30632a4309ff92d66eb4d07b9806106451edba30c21_prof);

        
        $__internal_8602462e1c5dbf9160d636f44874ec728cf55a7c7236d752bf3ef53e052e5bdf->leave($__internal_8602462e1c5dbf9160d636f44874ec728cf55a7c7236d752bf3ef53e052e5bdf_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_9bc230bb01ec3a1b816e2504b4a0c230818e07edbe52385c73fadec21e75bfe5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bc230bb01ec3a1b816e2504b4a0c230818e07edbe52385c73fadec21e75bfe5->enter($__internal_9bc230bb01ec3a1b816e2504b4a0c230818e07edbe52385c73fadec21e75bfe5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_260dcb6a4c75c249eff8057970166c028bdcf445f08fd8a812c9c79da23cd617 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_260dcb6a4c75c249eff8057970166c028bdcf445f08fd8a812c9c79da23cd617->enter($__internal_260dcb6a4c75c249eff8057970166c028bdcf445f08fd8a812c9c79da23cd617_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_260dcb6a4c75c249eff8057970166c028bdcf445f08fd8a812c9c79da23cd617->leave($__internal_260dcb6a4c75c249eff8057970166c028bdcf445f08fd8a812c9c79da23cd617_prof);

        
        $__internal_9bc230bb01ec3a1b816e2504b4a0c230818e07edbe52385c73fadec21e75bfe5->leave($__internal_9bc230bb01ec3a1b816e2504b4a0c230818e07edbe52385c73fadec21e75bfe5_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_fb32cae08f8769136b3bc31759a3df3eb48d35e54b13d84287128741d60823cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb32cae08f8769136b3bc31759a3df3eb48d35e54b13d84287128741d60823cf->enter($__internal_fb32cae08f8769136b3bc31759a3df3eb48d35e54b13d84287128741d60823cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_7ae4645bb25d006b6c20ab72b4401aaccb797f64c6e11bcb1ea4f9626f726f05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ae4645bb25d006b6c20ab72b4401aaccb797f64c6e11bcb1ea4f9626f726f05->enter($__internal_7ae4645bb25d006b6c20ab72b4401aaccb797f64c6e11bcb1ea4f9626f726f05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_7ae4645bb25d006b6c20ab72b4401aaccb797f64c6e11bcb1ea4f9626f726f05->leave($__internal_7ae4645bb25d006b6c20ab72b4401aaccb797f64c6e11bcb1ea4f9626f726f05_prof);

        
        $__internal_fb32cae08f8769136b3bc31759a3df3eb48d35e54b13d84287128741d60823cf->leave($__internal_fb32cae08f8769136b3bc31759a3df3eb48d35e54b13d84287128741d60823cf_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5de7a3cf1bae27bb5650535921e69aee24bc83570a11faf305e44dca56367ee9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5de7a3cf1bae27bb5650535921e69aee24bc83570a11faf305e44dca56367ee9->enter($__internal_5de7a3cf1bae27bb5650535921e69aee24bc83570a11faf305e44dca56367ee9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_af8f1ef97e85503574a2cb56e82821f5a7b7164020c2acbab52efaecd0237b65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af8f1ef97e85503574a2cb56e82821f5a7b7164020c2acbab52efaecd0237b65->enter($__internal_af8f1ef97e85503574a2cb56e82821f5a7b7164020c2acbab52efaecd0237b65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_af8f1ef97e85503574a2cb56e82821f5a7b7164020c2acbab52efaecd0237b65->leave($__internal_af8f1ef97e85503574a2cb56e82821f5a7b7164020c2acbab52efaecd0237b65_prof);

        
        $__internal_5de7a3cf1bae27bb5650535921e69aee24bc83570a11faf305e44dca56367ee9->leave($__internal_5de7a3cf1bae27bb5650535921e69aee24bc83570a11faf305e44dca56367ee9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/dmitriy/study/radionew/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
