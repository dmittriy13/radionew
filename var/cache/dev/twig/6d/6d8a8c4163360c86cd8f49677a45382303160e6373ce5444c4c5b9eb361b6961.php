<?php

/* base.html.twig */
class __TwigTemplate_ef6a598e82a5011af52d5b328f0cd2f8830bea5f4c88506ad9e23671aec9697a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0adb9a8e861cdfa543c371147f742eb9c2b5e91c3956ad85bbebaec7c1a71c03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0adb9a8e861cdfa543c371147f742eb9c2b5e91c3956ad85bbebaec7c1a71c03->enter($__internal_0adb9a8e861cdfa543c371147f742eb9c2b5e91c3956ad85bbebaec7c1a71c03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_7639e815f7cfb80062c778c4e5c82997513aa14b0fc746fe56a4d2408511331b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7639e815f7cfb80062c778c4e5c82997513aa14b0fc746fe56a4d2408511331b->enter($__internal_7639e815f7cfb80062c778c4e5c82997513aa14b0fc746fe56a4d2408511331b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_0adb9a8e861cdfa543c371147f742eb9c2b5e91c3956ad85bbebaec7c1a71c03->leave($__internal_0adb9a8e861cdfa543c371147f742eb9c2b5e91c3956ad85bbebaec7c1a71c03_prof);

        
        $__internal_7639e815f7cfb80062c778c4e5c82997513aa14b0fc746fe56a4d2408511331b->leave($__internal_7639e815f7cfb80062c778c4e5c82997513aa14b0fc746fe56a4d2408511331b_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_441fb865ea05960736f6c6b35b006dd6c276eccdb0617a43509640cdc8e8b7fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_441fb865ea05960736f6c6b35b006dd6c276eccdb0617a43509640cdc8e8b7fc->enter($__internal_441fb865ea05960736f6c6b35b006dd6c276eccdb0617a43509640cdc8e8b7fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6b2b597904483f1769a84be0cf0d89c7362726c206527abaf590cb38eea8f050 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b2b597904483f1769a84be0cf0d89c7362726c206527abaf590cb38eea8f050->enter($__internal_6b2b597904483f1769a84be0cf0d89c7362726c206527abaf590cb38eea8f050_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6b2b597904483f1769a84be0cf0d89c7362726c206527abaf590cb38eea8f050->leave($__internal_6b2b597904483f1769a84be0cf0d89c7362726c206527abaf590cb38eea8f050_prof);

        
        $__internal_441fb865ea05960736f6c6b35b006dd6c276eccdb0617a43509640cdc8e8b7fc->leave($__internal_441fb865ea05960736f6c6b35b006dd6c276eccdb0617a43509640cdc8e8b7fc_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a4bcb3300057e033ae478d73d018166cc9cf3040653567522cfb2b3516259c25 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4bcb3300057e033ae478d73d018166cc9cf3040653567522cfb2b3516259c25->enter($__internal_a4bcb3300057e033ae478d73d018166cc9cf3040653567522cfb2b3516259c25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_61aed70308de29eaf673d2f6c9ca20ab1f99921d18c7d2fd6faca4af55950080 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61aed70308de29eaf673d2f6c9ca20ab1f99921d18c7d2fd6faca4af55950080->enter($__internal_61aed70308de29eaf673d2f6c9ca20ab1f99921d18c7d2fd6faca4af55950080_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_61aed70308de29eaf673d2f6c9ca20ab1f99921d18c7d2fd6faca4af55950080->leave($__internal_61aed70308de29eaf673d2f6c9ca20ab1f99921d18c7d2fd6faca4af55950080_prof);

        
        $__internal_a4bcb3300057e033ae478d73d018166cc9cf3040653567522cfb2b3516259c25->leave($__internal_a4bcb3300057e033ae478d73d018166cc9cf3040653567522cfb2b3516259c25_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_aba86e7e0d7626777a05f0ac14d0e9f3fd7f330e02b7e21070848a4573ec921b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aba86e7e0d7626777a05f0ac14d0e9f3fd7f330e02b7e21070848a4573ec921b->enter($__internal_aba86e7e0d7626777a05f0ac14d0e9f3fd7f330e02b7e21070848a4573ec921b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c306a57fef3f3276a94ff97ed2efe88667678e75592822968794b78978afd04d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c306a57fef3f3276a94ff97ed2efe88667678e75592822968794b78978afd04d->enter($__internal_c306a57fef3f3276a94ff97ed2efe88667678e75592822968794b78978afd04d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c306a57fef3f3276a94ff97ed2efe88667678e75592822968794b78978afd04d->leave($__internal_c306a57fef3f3276a94ff97ed2efe88667678e75592822968794b78978afd04d_prof);

        
        $__internal_aba86e7e0d7626777a05f0ac14d0e9f3fd7f330e02b7e21070848a4573ec921b->leave($__internal_aba86e7e0d7626777a05f0ac14d0e9f3fd7f330e02b7e21070848a4573ec921b_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1d3d30e5b6abfe1b28d718eb09fa6e252b28d716e504d90265c0a318b87be9c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d3d30e5b6abfe1b28d718eb09fa6e252b28d716e504d90265c0a318b87be9c3->enter($__internal_1d3d30e5b6abfe1b28d718eb09fa6e252b28d716e504d90265c0a318b87be9c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d9c2f93396289e8480aceda837cb677dd9d5167dd98448fdd22d9414d4976d00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9c2f93396289e8480aceda837cb677dd9d5167dd98448fdd22d9414d4976d00->enter($__internal_d9c2f93396289e8480aceda837cb677dd9d5167dd98448fdd22d9414d4976d00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d9c2f93396289e8480aceda837cb677dd9d5167dd98448fdd22d9414d4976d00->leave($__internal_d9c2f93396289e8480aceda837cb677dd9d5167dd98448fdd22d9414d4976d00_prof);

        
        $__internal_1d3d30e5b6abfe1b28d718eb09fa6e252b28d716e504d90265c0a318b87be9c3->leave($__internal_1d3d30e5b6abfe1b28d718eb09fa6e252b28d716e504d90265c0a318b87be9c3_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/dmitriy/study/radionew/app/Resources/views/base.html.twig");
    }
}
