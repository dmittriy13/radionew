<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_c609787acd24ec0b4b1a361d4531bbba377252f376fc0c3641ca193572b7906f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a23c421e8f5a170bacd3449f41244f1f2979a935c8ab134cb59679e5ccb5a556 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a23c421e8f5a170bacd3449f41244f1f2979a935c8ab134cb59679e5ccb5a556->enter($__internal_a23c421e8f5a170bacd3449f41244f1f2979a935c8ab134cb59679e5ccb5a556_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_d79adda5365518a6359d442a63e0dba056c349aa499dddf707ad50f8cca1060b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d79adda5365518a6359d442a63e0dba056c349aa499dddf707ad50f8cca1060b->enter($__internal_d79adda5365518a6359d442a63e0dba056c349aa499dddf707ad50f8cca1060b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a23c421e8f5a170bacd3449f41244f1f2979a935c8ab134cb59679e5ccb5a556->leave($__internal_a23c421e8f5a170bacd3449f41244f1f2979a935c8ab134cb59679e5ccb5a556_prof);

        
        $__internal_d79adda5365518a6359d442a63e0dba056c349aa499dddf707ad50f8cca1060b->leave($__internal_d79adda5365518a6359d442a63e0dba056c349aa499dddf707ad50f8cca1060b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b34c6de64e0260c5debdac7eb2377480508e513d1501eaca213a7629a1d06728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b34c6de64e0260c5debdac7eb2377480508e513d1501eaca213a7629a1d06728->enter($__internal_b34c6de64e0260c5debdac7eb2377480508e513d1501eaca213a7629a1d06728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ac78b7650595e690d8770dde9d5cb5e78de69b0fa7a2823c6672cfaa7f284e14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac78b7650595e690d8770dde9d5cb5e78de69b0fa7a2823c6672cfaa7f284e14->enter($__internal_ac78b7650595e690d8770dde9d5cb5e78de69b0fa7a2823c6672cfaa7f284e14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_ac78b7650595e690d8770dde9d5cb5e78de69b0fa7a2823c6672cfaa7f284e14->leave($__internal_ac78b7650595e690d8770dde9d5cb5e78de69b0fa7a2823c6672cfaa7f284e14_prof);

        
        $__internal_b34c6de64e0260c5debdac7eb2377480508e513d1501eaca213a7629a1d06728->leave($__internal_b34c6de64e0260c5debdac7eb2377480508e513d1501eaca213a7629a1d06728_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
