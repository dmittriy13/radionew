<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_5ace0514ac51dddf7015e278581d1ccbc34e9ce9c04fa0c5adac0bc55a3fad5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95a2ea113e796cbffec8d1ce155722a42681b26b5cab860725667e0616860537 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95a2ea113e796cbffec8d1ce155722a42681b26b5cab860725667e0616860537->enter($__internal_95a2ea113e796cbffec8d1ce155722a42681b26b5cab860725667e0616860537_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_e0dad7b4f28e27e73af48172e2820dc67b9719fc1107a1d5fb2d1ead5d7ecc1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0dad7b4f28e27e73af48172e2820dc67b9719fc1107a1d5fb2d1ead5d7ecc1f->enter($__internal_e0dad7b4f28e27e73af48172e2820dc67b9719fc1107a1d5fb2d1ead5d7ecc1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95a2ea113e796cbffec8d1ce155722a42681b26b5cab860725667e0616860537->leave($__internal_95a2ea113e796cbffec8d1ce155722a42681b26b5cab860725667e0616860537_prof);

        
        $__internal_e0dad7b4f28e27e73af48172e2820dc67b9719fc1107a1d5fb2d1ead5d7ecc1f->leave($__internal_e0dad7b4f28e27e73af48172e2820dc67b9719fc1107a1d5fb2d1ead5d7ecc1f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3cc4f8d885e9f3b1333a234933edf579a0aa31467d38072f61fe43d4382f83e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cc4f8d885e9f3b1333a234933edf579a0aa31467d38072f61fe43d4382f83e0->enter($__internal_3cc4f8d885e9f3b1333a234933edf579a0aa31467d38072f61fe43d4382f83e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4136b8015ed16f573fe05ef68d3a2212614411ebc91c883fb1c77d1bb069e958 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4136b8015ed16f573fe05ef68d3a2212614411ebc91c883fb1c77d1bb069e958->enter($__internal_4136b8015ed16f573fe05ef68d3a2212614411ebc91c883fb1c77d1bb069e958_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_4136b8015ed16f573fe05ef68d3a2212614411ebc91c883fb1c77d1bb069e958->leave($__internal_4136b8015ed16f573fe05ef68d3a2212614411ebc91c883fb1c77d1bb069e958_prof);

        
        $__internal_3cc4f8d885e9f3b1333a234933edf579a0aa31467d38072f61fe43d4382f83e0->leave($__internal_3cc4f8d885e9f3b1333a234933edf579a0aa31467d38072f61fe43d4382f83e0_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_d6cc9c1008fe4889a8c3f2d9224e7c158126a45b825a4ed67e6a53cc79304137 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6cc9c1008fe4889a8c3f2d9224e7c158126a45b825a4ed67e6a53cc79304137->enter($__internal_d6cc9c1008fe4889a8c3f2d9224e7c158126a45b825a4ed67e6a53cc79304137_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8d9f20fe6b194309dd71330d832c93b1292c5659ec1931e6c6d2375b326bcd55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d9f20fe6b194309dd71330d832c93b1292c5659ec1931e6c6d2375b326bcd55->enter($__internal_8d9f20fe6b194309dd71330d832c93b1292c5659ec1931e6c6d2375b326bcd55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_8d9f20fe6b194309dd71330d832c93b1292c5659ec1931e6c6d2375b326bcd55->leave($__internal_8d9f20fe6b194309dd71330d832c93b1292c5659ec1931e6c6d2375b326bcd55_prof);

        
        $__internal_d6cc9c1008fe4889a8c3f2d9224e7c158126a45b825a4ed67e6a53cc79304137->leave($__internal_d6cc9c1008fe4889a8c3f2d9224e7c158126a45b825a4ed67e6a53cc79304137_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_312b514dd3256101257fd7b3940a007e0a9bde7106c4445d5f72e37ae95e8e0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_312b514dd3256101257fd7b3940a007e0a9bde7106c4445d5f72e37ae95e8e0b->enter($__internal_312b514dd3256101257fd7b3940a007e0a9bde7106c4445d5f72e37ae95e8e0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_de1435bc5ed3ff3acda679d6464d765460021d1b35aaae204aeba207218d2473 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de1435bc5ed3ff3acda679d6464d765460021d1b35aaae204aeba207218d2473->enter($__internal_de1435bc5ed3ff3acda679d6464d765460021d1b35aaae204aeba207218d2473_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_de1435bc5ed3ff3acda679d6464d765460021d1b35aaae204aeba207218d2473->leave($__internal_de1435bc5ed3ff3acda679d6464d765460021d1b35aaae204aeba207218d2473_prof);

        
        $__internal_312b514dd3256101257fd7b3940a007e0a9bde7106c4445d5f72e37ae95e8e0b->leave($__internal_312b514dd3256101257fd7b3940a007e0a9bde7106c4445d5f72e37ae95e8e0b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/dmitriy/study/radionew/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
