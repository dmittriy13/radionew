<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6c775e4c4ccdb3af11f3aa2031a382d5c0ed87f26f326c1f292fdbc62fcd4060 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4fc978ebfa8beaefd46b4b5407ba5de5c2c8c3172a11c83474f62f3fb5e7206b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4fc978ebfa8beaefd46b4b5407ba5de5c2c8c3172a11c83474f62f3fb5e7206b->enter($__internal_4fc978ebfa8beaefd46b4b5407ba5de5c2c8c3172a11c83474f62f3fb5e7206b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_b28a9a5520b4ab30889d0fd448c3a0d3dc91b7da8979bceeebbcf3473ec6a77b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b28a9a5520b4ab30889d0fd448c3a0d3dc91b7da8979bceeebbcf3473ec6a77b->enter($__internal_b28a9a5520b4ab30889d0fd448c3a0d3dc91b7da8979bceeebbcf3473ec6a77b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4fc978ebfa8beaefd46b4b5407ba5de5c2c8c3172a11c83474f62f3fb5e7206b->leave($__internal_4fc978ebfa8beaefd46b4b5407ba5de5c2c8c3172a11c83474f62f3fb5e7206b_prof);

        
        $__internal_b28a9a5520b4ab30889d0fd448c3a0d3dc91b7da8979bceeebbcf3473ec6a77b->leave($__internal_b28a9a5520b4ab30889d0fd448c3a0d3dc91b7da8979bceeebbcf3473ec6a77b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_892a5b890911fe4ec3a44d61118d5a8fbcc8d8df6a5fa4a64c07811a3e9a390b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_892a5b890911fe4ec3a44d61118d5a8fbcc8d8df6a5fa4a64c07811a3e9a390b->enter($__internal_892a5b890911fe4ec3a44d61118d5a8fbcc8d8df6a5fa4a64c07811a3e9a390b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_59c335f5fd119c17f4600bc89c657399e327dc4b4d3eb3533a384a16c091028f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59c335f5fd119c17f4600bc89c657399e327dc4b4d3eb3533a384a16c091028f->enter($__internal_59c335f5fd119c17f4600bc89c657399e327dc4b4d3eb3533a384a16c091028f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_59c335f5fd119c17f4600bc89c657399e327dc4b4d3eb3533a384a16c091028f->leave($__internal_59c335f5fd119c17f4600bc89c657399e327dc4b4d3eb3533a384a16c091028f_prof);

        
        $__internal_892a5b890911fe4ec3a44d61118d5a8fbcc8d8df6a5fa4a64c07811a3e9a390b->leave($__internal_892a5b890911fe4ec3a44d61118d5a8fbcc8d8df6a5fa4a64c07811a3e9a390b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a0547816b17d428b9ab23c2118ded686f7c6d9d1ba1ac3ae88da33761c509af9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0547816b17d428b9ab23c2118ded686f7c6d9d1ba1ac3ae88da33761c509af9->enter($__internal_a0547816b17d428b9ab23c2118ded686f7c6d9d1ba1ac3ae88da33761c509af9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_5fa961f38e5a76f17c0a9aeea8381e45f1fadbe579dbdc5a2f9281b504d516dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fa961f38e5a76f17c0a9aeea8381e45f1fadbe579dbdc5a2f9281b504d516dd->enter($__internal_5fa961f38e5a76f17c0a9aeea8381e45f1fadbe579dbdc5a2f9281b504d516dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_5fa961f38e5a76f17c0a9aeea8381e45f1fadbe579dbdc5a2f9281b504d516dd->leave($__internal_5fa961f38e5a76f17c0a9aeea8381e45f1fadbe579dbdc5a2f9281b504d516dd_prof);

        
        $__internal_a0547816b17d428b9ab23c2118ded686f7c6d9d1ba1ac3ae88da33761c509af9->leave($__internal_a0547816b17d428b9ab23c2118ded686f7c6d9d1ba1ac3ae88da33761c509af9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_edab558d3808819ce217f0cb7c8d9413f15924643b70e498b6b28f20c0fe12e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edab558d3808819ce217f0cb7c8d9413f15924643b70e498b6b28f20c0fe12e4->enter($__internal_edab558d3808819ce217f0cb7c8d9413f15924643b70e498b6b28f20c0fe12e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6486c53fd561068ef9cb508ab5f6927d0bbb6ba04b3bee4faaa0337b47dc77f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6486c53fd561068ef9cb508ab5f6927d0bbb6ba04b3bee4faaa0337b47dc77f6->enter($__internal_6486c53fd561068ef9cb508ab5f6927d0bbb6ba04b3bee4faaa0337b47dc77f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_6486c53fd561068ef9cb508ab5f6927d0bbb6ba04b3bee4faaa0337b47dc77f6->leave($__internal_6486c53fd561068ef9cb508ab5f6927d0bbb6ba04b3bee4faaa0337b47dc77f6_prof);

        
        $__internal_edab558d3808819ce217f0cb7c8d9413f15924643b70e498b6b28f20c0fe12e4->leave($__internal_edab558d3808819ce217f0cb7c8d9413f15924643b70e498b6b28f20c0fe12e4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/dmitriy/study/radionew/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
