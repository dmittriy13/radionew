<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_50765ea9f3b79110318ff17402f2becdc00499c3164d30aa3fa085ff94ccd376 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_89fd607a71c900bb4bec49749ba9ac5890a65b66dcb5adb1d1ceb6456e15f59b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89fd607a71c900bb4bec49749ba9ac5890a65b66dcb5adb1d1ceb6456e15f59b->enter($__internal_89fd607a71c900bb4bec49749ba9ac5890a65b66dcb5adb1d1ceb6456e15f59b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $__internal_9db8ecc7faff79efe69020d0f354fb514caade31e58429f4348edd610ac93393 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9db8ecc7faff79efe69020d0f354fb514caade31e58429f4348edd610ac93393->enter($__internal_9db8ecc7faff79efe69020d0f354fb514caade31e58429f4348edd610ac93393_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_89fd607a71c900bb4bec49749ba9ac5890a65b66dcb5adb1d1ceb6456e15f59b->leave($__internal_89fd607a71c900bb4bec49749ba9ac5890a65b66dcb5adb1d1ceb6456e15f59b_prof);

        
        $__internal_9db8ecc7faff79efe69020d0f354fb514caade31e58429f4348edd610ac93393->leave($__internal_9db8ecc7faff79efe69020d0f354fb514caade31e58429f4348edd610ac93393_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1929babacc27ef43c06fc7233acebc13b1a5f1d0b495e1cca0338c45216b386e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1929babacc27ef43c06fc7233acebc13b1a5f1d0b495e1cca0338c45216b386e->enter($__internal_1929babacc27ef43c06fc7233acebc13b1a5f1d0b495e1cca0338c45216b386e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_69ec6434872cb5cbcf15bd20db2cc2ab1900b70fc4288389d74015091e78ea83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69ec6434872cb5cbcf15bd20db2cc2ab1900b70fc4288389d74015091e78ea83->enter($__internal_69ec6434872cb5cbcf15bd20db2cc2ab1900b70fc4288389d74015091e78ea83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_69ec6434872cb5cbcf15bd20db2cc2ab1900b70fc4288389d74015091e78ea83->leave($__internal_69ec6434872cb5cbcf15bd20db2cc2ab1900b70fc4288389d74015091e78ea83_prof);

        
        $__internal_1929babacc27ef43c06fc7233acebc13b1a5f1d0b495e1cca0338c45216b386e->leave($__internal_1929babacc27ef43c06fc7233acebc13b1a5f1d0b495e1cca0338c45216b386e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/ChangePassword/change_password.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
