<?php

/* @App/layout.html.twig */
class __TwigTemplate_d11be7f112df34dac7f298d02402034cffd9982e10bafccf5f4d0781a7ebfc6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0eb8fc575acf13111971ff17eac42d17332482d82a62deca8bd91cfc6c85cd91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0eb8fc575acf13111971ff17eac42d17332482d82a62deca8bd91cfc6c85cd91->enter($__internal_0eb8fc575acf13111971ff17eac42d17332482d82a62deca8bd91cfc6c85cd91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/layout.html.twig"));

        $__internal_6fe8ba72f048e1b6213576e72b84620c69d8a02ada9785cbacc7a7b9c3ad9b1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6fe8ba72f048e1b6213576e72b84620c69d8a02ada9785cbacc7a7b9c3ad9b1f->enter($__internal_6fe8ba72f048e1b6213576e72b84620c69d8a02ada9785cbacc7a7b9c3ad9b1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        // line 11
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "</body>
</html>
";
        
        $__internal_0eb8fc575acf13111971ff17eac42d17332482d82a62deca8bd91cfc6c85cd91->leave($__internal_0eb8fc575acf13111971ff17eac42d17332482d82a62deca8bd91cfc6c85cd91_prof);

        
        $__internal_6fe8ba72f048e1b6213576e72b84620c69d8a02ada9785cbacc7a7b9c3ad9b1f->leave($__internal_6fe8ba72f048e1b6213576e72b84620c69d8a02ada9785cbacc7a7b9c3ad9b1f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_009e82d97d3f9b0f526e920b9daf48b9307c1703ac7418cc7b116d58362233d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_009e82d97d3f9b0f526e920b9daf48b9307c1703ac7418cc7b116d58362233d3->enter($__internal_009e82d97d3f9b0f526e920b9daf48b9307c1703ac7418cc7b116d58362233d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e5a132b51ad533b793605cba457779307a0fa8859a905d555aa1345135618c57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5a132b51ad533b793605cba457779307a0fa8859a905d555aa1345135618c57->enter($__internal_e5a132b51ad533b793605cba457779307a0fa8859a905d555aa1345135618c57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_e5a132b51ad533b793605cba457779307a0fa8859a905d555aa1345135618c57->leave($__internal_e5a132b51ad533b793605cba457779307a0fa8859a905d555aa1345135618c57_prof);

        
        $__internal_009e82d97d3f9b0f526e920b9daf48b9307c1703ac7418cc7b116d58362233d3->leave($__internal_009e82d97d3f9b0f526e920b9daf48b9307c1703ac7418cc7b116d58362233d3_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_97095bbb15f838665e1d9c0fc8a23b0f4d21d0028dd9585e232addd82ac60475 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97095bbb15f838665e1d9c0fc8a23b0f4d21d0028dd9585e232addd82ac60475->enter($__internal_97095bbb15f838665e1d9c0fc8a23b0f4d21d0028dd9585e232addd82ac60475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_990c6451d0ae8c05015dfa9b4a35fa39832e43c664e1fc97a6ae44b39847359b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_990c6451d0ae8c05015dfa9b4a35fa39832e43c664e1fc97a6ae44b39847359b->enter($__internal_990c6451d0ae8c05015dfa9b4a35fa39832e43c664e1fc97a6ae44b39847359b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_990c6451d0ae8c05015dfa9b4a35fa39832e43c664e1fc97a6ae44b39847359b->leave($__internal_990c6451d0ae8c05015dfa9b4a35fa39832e43c664e1fc97a6ae44b39847359b_prof);

        
        $__internal_97095bbb15f838665e1d9c0fc8a23b0f4d21d0028dd9585e232addd82ac60475->leave($__internal_97095bbb15f838665e1d9c0fc8a23b0f4d21d0028dd9585e232addd82ac60475_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_888f373f937805af6231be9db6b2d87a0d9a43ee3f22b839ca1f2542e56d7a43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_888f373f937805af6231be9db6b2d87a0d9a43ee3f22b839ca1f2542e56d7a43->enter($__internal_888f373f937805af6231be9db6b2d87a0d9a43ee3f22b839ca1f2542e56d7a43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_a9510ebee97679b2e1430a5d225eb7fbf111ccdb7881b9d2d8b967c0b0a30330 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9510ebee97679b2e1430a5d225eb7fbf111ccdb7881b9d2d8b967c0b0a30330->enter($__internal_a9510ebee97679b2e1430a5d225eb7fbf111ccdb7881b9d2d8b967c0b0a30330_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_a9510ebee97679b2e1430a5d225eb7fbf111ccdb7881b9d2d8b967c0b0a30330->leave($__internal_a9510ebee97679b2e1430a5d225eb7fbf111ccdb7881b9d2d8b967c0b0a30330_prof);

        
        $__internal_888f373f937805af6231be9db6b2d87a0d9a43ee3f22b839ca1f2542e56d7a43->leave($__internal_888f373f937805af6231be9db6b2d87a0d9a43ee3f22b839ca1f2542e56d7a43_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a9e988ff90936c4568a37689a901a0669ae15039cacb53b3b5b5c2ff61e95a15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9e988ff90936c4568a37689a901a0669ae15039cacb53b3b5b5c2ff61e95a15->enter($__internal_a9e988ff90936c4568a37689a901a0669ae15039cacb53b3b5b5c2ff61e95a15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5ccf5b84f52c1f61d920ffa0b407a5b602ee8d3c21e21be0717673e33ddbaa0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ccf5b84f52c1f61d920ffa0b407a5b602ee8d3c21e21be0717673e33ddbaa0d->enter($__internal_5ccf5b84f52c1f61d920ffa0b407a5b602ee8d3c21e21be0717673e33ddbaa0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5ccf5b84f52c1f61d920ffa0b407a5b602ee8d3c21e21be0717673e33ddbaa0d->leave($__internal_5ccf5b84f52c1f61d920ffa0b407a5b602ee8d3c21e21be0717673e33ddbaa0d_prof);

        
        $__internal_a9e988ff90936c4568a37689a901a0669ae15039cacb53b3b5b5c2ff61e95a15->leave($__internal_a9e988ff90936c4568a37689a901a0669ae15039cacb53b3b5b5c2ff61e95a15_prof);

    }

    public function getTemplateName()
    {
        return "@App/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 11,  98 => 10,  81 => 6,  64 => 5,  52 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>{% block title %}{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
</head>
<body>
{% block content %}{% endblock %}
{% block javascripts %}{% endblock %}
</body>
</html>
", "@App/layout.html.twig", "/home/dmitriy/study/radionew/src/AppBundle/Resources/views/layout.html.twig");
    }
}
