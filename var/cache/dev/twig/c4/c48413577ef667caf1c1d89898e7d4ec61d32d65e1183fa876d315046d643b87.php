<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_2e366d753fa19c767eb719a9bdec877ec3bcc387f06a26830fc81bf76364037d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84a3ec75bc3124ece4a80d4a9301578e0c450f600fd9c57b439a466701e9d850 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84a3ec75bc3124ece4a80d4a9301578e0c450f600fd9c57b439a466701e9d850->enter($__internal_84a3ec75bc3124ece4a80d4a9301578e0c450f600fd9c57b439a466701e9d850_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_a58ece444e5e40fe081662115da681127234a5aa92b92c3a715cd0536b086dae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a58ece444e5e40fe081662115da681127234a5aa92b92c3a715cd0536b086dae->enter($__internal_a58ece444e5e40fe081662115da681127234a5aa92b92c3a715cd0536b086dae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_84a3ec75bc3124ece4a80d4a9301578e0c450f600fd9c57b439a466701e9d850->leave($__internal_84a3ec75bc3124ece4a80d4a9301578e0c450f600fd9c57b439a466701e9d850_prof);

        
        $__internal_a58ece444e5e40fe081662115da681127234a5aa92b92c3a715cd0536b086dae->leave($__internal_a58ece444e5e40fe081662115da681127234a5aa92b92c3a715cd0536b086dae_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cf4ee17a19fc6bcd8a15a4b4b3f1830216a75051c11ee2c9506774f5205b953d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf4ee17a19fc6bcd8a15a4b4b3f1830216a75051c11ee2c9506774f5205b953d->enter($__internal_cf4ee17a19fc6bcd8a15a4b4b3f1830216a75051c11ee2c9506774f5205b953d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bd0a8943b5155b07068cbec96b728447c981d0e44ff0ef5ee557f6eb3e8a91fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd0a8943b5155b07068cbec96b728447c981d0e44ff0ef5ee557f6eb3e8a91fc->enter($__internal_bd0a8943b5155b07068cbec96b728447c981d0e44ff0ef5ee557f6eb3e8a91fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_bd0a8943b5155b07068cbec96b728447c981d0e44ff0ef5ee557f6eb3e8a91fc->leave($__internal_bd0a8943b5155b07068cbec96b728447c981d0e44ff0ef5ee557f6eb3e8a91fc_prof);

        
        $__internal_cf4ee17a19fc6bcd8a15a4b4b3f1830216a75051c11ee2c9506774f5205b953d->leave($__internal_cf4ee17a19fc6bcd8a15a4b4b3f1830216a75051c11ee2c9506774f5205b953d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "/home/dmitriy/study/radionew/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
