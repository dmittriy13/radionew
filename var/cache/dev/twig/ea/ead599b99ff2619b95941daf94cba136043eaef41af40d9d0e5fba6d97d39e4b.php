<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_2818adb03de0349061047d8de12e1cb1f6b314a5fb9b9431c7361d075fded912 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc8ca2f85401ee86c707e4ff05e6df8009b0bba2af6a0b9eee2e86f6b45881d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc8ca2f85401ee86c707e4ff05e6df8009b0bba2af6a0b9eee2e86f6b45881d1->enter($__internal_cc8ca2f85401ee86c707e4ff05e6df8009b0bba2af6a0b9eee2e86f6b45881d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_6c960da58179ec87a4b5784102a761795b0a00f6cd54be33cbc89acc359b2539 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c960da58179ec87a4b5784102a761795b0a00f6cd54be33cbc89acc359b2539->enter($__internal_6c960da58179ec87a4b5784102a761795b0a00f6cd54be33cbc89acc359b2539_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_cc8ca2f85401ee86c707e4ff05e6df8009b0bba2af6a0b9eee2e86f6b45881d1->leave($__internal_cc8ca2f85401ee86c707e4ff05e6df8009b0bba2af6a0b9eee2e86f6b45881d1_prof);

        
        $__internal_6c960da58179ec87a4b5784102a761795b0a00f6cd54be33cbc89acc359b2539->leave($__internal_6c960da58179ec87a4b5784102a761795b0a00f6cd54be33cbc89acc359b2539_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/home/dmitriy/study/radionew/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
