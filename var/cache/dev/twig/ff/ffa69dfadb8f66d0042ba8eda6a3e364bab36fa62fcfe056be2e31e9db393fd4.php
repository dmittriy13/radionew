<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_5bb381d2016f4fb31ed76213b789caa25a3f81b0f29e559643b0dd7ce9f134c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5e676517ba3b81908055204010d88c4fb642e263afd8e2df60204bf39e06130 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5e676517ba3b81908055204010d88c4fb642e263afd8e2df60204bf39e06130->enter($__internal_a5e676517ba3b81908055204010d88c4fb642e263afd8e2df60204bf39e06130_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_7dd2a1e823f5a622099944436e107991a9ea48a809ad4869d49a3f471c8ae15e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7dd2a1e823f5a622099944436e107991a9ea48a809ad4869d49a3f471c8ae15e->enter($__internal_7dd2a1e823f5a622099944436e107991a9ea48a809ad4869d49a3f471c8ae15e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_a5e676517ba3b81908055204010d88c4fb642e263afd8e2df60204bf39e06130->leave($__internal_a5e676517ba3b81908055204010d88c4fb642e263afd8e2df60204bf39e06130_prof);

        
        $__internal_7dd2a1e823f5a622099944436e107991a9ea48a809ad4869d49a3f471c8ae15e->leave($__internal_7dd2a1e823f5a622099944436e107991a9ea48a809ad4869d49a3f471c8ae15e_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ca55c773bf6a1a62a19685cff1fca3e39c36e1e1b9446d2bc2cd48ba7cd32460 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca55c773bf6a1a62a19685cff1fca3e39c36e1e1b9446d2bc2cd48ba7cd32460->enter($__internal_ca55c773bf6a1a62a19685cff1fca3e39c36e1e1b9446d2bc2cd48ba7cd32460_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f99190d8d029e04b58cd2e3ec61823efaa907fb402818c8dfb137abbe9ce5609 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f99190d8d029e04b58cd2e3ec61823efaa907fb402818c8dfb137abbe9ce5609->enter($__internal_f99190d8d029e04b58cd2e3ec61823efaa907fb402818c8dfb137abbe9ce5609_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_f99190d8d029e04b58cd2e3ec61823efaa907fb402818c8dfb137abbe9ce5609->leave($__internal_f99190d8d029e04b58cd2e3ec61823efaa907fb402818c8dfb137abbe9ce5609_prof);

        
        $__internal_ca55c773bf6a1a62a19685cff1fca3e39c36e1e1b9446d2bc2cd48ba7cd32460->leave($__internal_ca55c773bf6a1a62a19685cff1fca3e39c36e1e1b9446d2bc2cd48ba7cd32460_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_22e153c7ff3568aca763bb3ac778e4297131641e5ff7e80d0be776d9256a220d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22e153c7ff3568aca763bb3ac778e4297131641e5ff7e80d0be776d9256a220d->enter($__internal_22e153c7ff3568aca763bb3ac778e4297131641e5ff7e80d0be776d9256a220d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_346e82cfdd0ab58b531648afbbe17dead6e502aceea2127145b47f0e0d2edffc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_346e82cfdd0ab58b531648afbbe17dead6e502aceea2127145b47f0e0d2edffc->enter($__internal_346e82cfdd0ab58b531648afbbe17dead6e502aceea2127145b47f0e0d2edffc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_346e82cfdd0ab58b531648afbbe17dead6e502aceea2127145b47f0e0d2edffc->leave($__internal_346e82cfdd0ab58b531648afbbe17dead6e502aceea2127145b47f0e0d2edffc_prof);

        
        $__internal_22e153c7ff3568aca763bb3ac778e4297131641e5ff7e80d0be776d9256a220d->leave($__internal_22e153c7ff3568aca763bb3ac778e4297131641e5ff7e80d0be776d9256a220d_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_e66d5511d786c53eb7fed485722a5437366cab5ef76638b892f066e9a76992bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e66d5511d786c53eb7fed485722a5437366cab5ef76638b892f066e9a76992bb->enter($__internal_e66d5511d786c53eb7fed485722a5437366cab5ef76638b892f066e9a76992bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4289ebcdad7029d01a855f3bed4b9752a153b0162c8660611619d98716c0f138 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4289ebcdad7029d01a855f3bed4b9752a153b0162c8660611619d98716c0f138->enter($__internal_4289ebcdad7029d01a855f3bed4b9752a153b0162c8660611619d98716c0f138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_4289ebcdad7029d01a855f3bed4b9752a153b0162c8660611619d98716c0f138->leave($__internal_4289ebcdad7029d01a855f3bed4b9752a153b0162c8660611619d98716c0f138_prof);

        
        $__internal_e66d5511d786c53eb7fed485722a5437366cab5ef76638b892f066e9a76992bb->leave($__internal_e66d5511d786c53eb7fed485722a5437366cab5ef76638b892f066e9a76992bb_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/dmitriy/study/radionew/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
